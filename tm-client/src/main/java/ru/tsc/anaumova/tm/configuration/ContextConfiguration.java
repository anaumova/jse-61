package ru.tsc.anaumova.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.anaumova.tm.api.endpoint.*;
import ru.tsc.anaumova.tm.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.tsc.anaumova.tm")
public class ContextConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public IDomainEndpoint adminEndpoint() {
        return IDomainEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

    @Bean
    @NotNull
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

    @Bean
    @NotNull
    public IProjectTaskEndpoint projectTaskEndpoint() {
        return IProjectTaskEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {
        return IDomainEndpoint.newInstance(propertyService.getServerHost(), String.valueOf(propertyService.getServerPort()));
    }

}